Name:           clash
Version:        1.0.0
Release:        2%{?dist}
Summary:        A rule-based tunnel in Go.


License:        MIT
URL:            https://github.com/Dreamacro/clash
Source0:        https://github.com/Dreamacro/clash/releases/download/v%{version}/clash-linux-amd64-v%{version}.gz
Source1:        clash@.service
Source2:        clash.service
BuildRequires:  systemd-devel
%{?systemd_requires}


%description
A rule-based tunnel in Go.

%prep
%setup -n clash -c clash


%install
install -m 0755 -D clash-linux-amd64 %{buildroot}%{_bindir}/clash
install -m 0644 -D %{SOURCE1} %{buildroot}/usr/lib/systemd/system/clash@.service
install -m 0644 -D %{SOURCE2} %{buildroot}/usr/lib/systemd/system/clash.service


%preun
%systemd_preun clash.service clash@*.service


%postun
%systemd_postun_with_restart clash.service clash@*.service


%files
%{_bindir}/clash
/usr/lib/systemd/system/clash.service
/usr/lib/systemd/system/clash@.service


%changelog
* Thu Jul 23 2020 proletarius101 <proletarius101@protonmail.com> 1.0.0-1
- Initial RPM release

︽
